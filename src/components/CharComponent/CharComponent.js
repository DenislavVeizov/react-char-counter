import "./CharComponent.css";
import React from "react";

const CharComponent = (props) => {
    return (
        <div className='CharComponent' onClick={props.removeChar}>
            {props.char}
        </div>
    )
}

export default CharComponent
