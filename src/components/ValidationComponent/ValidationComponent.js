import React from "react";

const ValidationComponent = ({textLength}) => {
    const emptyText = textLength === 0;
    const tooLong = textLength > 20
    const tooShort = textLength < 5

    let validationText = 'Text is valid';
    if (emptyText) {
        validationText = '';
    } else if (tooLong) {
        validationText = 'Text too long: ' + textLength;
    } else if (tooShort) {
        validationText = 'Text too short: ' + textLength;
    }

    const validaitonTextCss = () => {
        let color = 'green';
        if (tooLong || tooShort) {
            color = 'red'
        }
        return {
            color: color
        }
    }

    return (
        <div>
            <p style={validaitonTextCss()}>{validationText}</p>
        </div>
    )
}

export default ValidationComponent
