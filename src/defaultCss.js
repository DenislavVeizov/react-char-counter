export const textCenter = {
    textAlign: 'center',
}

export const marginTop20 = {
    marginTop: '20px'
}

export const marginRight20 = {
    marginRight: '20px'
}
