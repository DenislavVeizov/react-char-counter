import './App.css';
import React, {useState} from "react";
import ValidationComponent from "./components/ValidationComponent/ValidationComponent";
import {marginTop20, marginRight20, textCenter} from "./defaultCss";
import CharComponent from "./components/CharComponent/CharComponent";

const App = (props) => {
    const [enteredText, setEnteredText] = useState('');

    const removeChar = (index) => {
        const newText = [...enteredText].filter((item, i) => index !== i)
        setEnteredText(newText.join(''));
    }

    const changeEnteredTextHandler = (event) => {
        const value = event.target.value;
        setEnteredText(value);
    }

    const charComponentArray = [...enteredText].map((item, index) => {
        return <CharComponent key={index} char={item} removeChar={() => removeChar(index)}/>
    })

    return (
        <div style={{...textCenter, ...marginTop20}}>
            <div>
                <label style={marginRight20} htmlFor='inputField'>Please enter text</label>
                <input value={enteredText} onChange={changeEnteredTextHandler} id='inputField' type="text"/>
            </div>
            <div style={marginTop20}>
                <p>Entered text length: {enteredText.length}</p>
            </div>
            <ValidationComponent textLength={enteredText.length}></ValidationComponent>
            {charComponentArray}
        </div>
    )
}

export default App;
